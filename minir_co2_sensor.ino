/*****************************************************************************
   Includes
 ****************************************************************************/

#include <SoftwareSerial.h>
#include <TheThingsNetwork.h>

/*****************************************************************************
   Definitions and Global Variables
 ****************************************************************************/

 /* Set your AppEUI and AppKey here */
const char *appEui = "70B3D57ED001014F";
const char *appKey = "2A4F2DF863CD195A3269517541EBFABE";

/* Serial debugging */
#define debugSerial Serial
#define DEBUG_SERIAL_BAUD 9600

/* For talking to the CO2 sensor */
#define CO2_SERIAL_RX_PIN 8
#define CO2_SERIAL_TX_PIN 9
#define CO2_SERIAL_BAUD 9600
SoftwareSerial co2Serial(CO2_SERIAL_RX_PIN, CO2_SERIAL_TX_PIN);

/* LoRa stuff */
#define loraSerial Serial1
#define freqPlan TTN_FP_US915
#define LORA_SERIAL_BAUD 57600
TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

/* MINIR-5% CO2 Sensor State */
typedef struct {
  float temperature;
  float humidity;
  float barometric_pressure;
  int uncompensated_co2;
  int compensated_co2;
} co2_sensor;

/* Holds the current state of the CO2 sensor */
co2_sensor co2_sensor_state;

/*****************************************************************************
   Functions
 ****************************************************************************/

int send_command_to_co2_sensor(char command, char *response)
{
  int num_bytes_received = 0;
  char response_buffer[32];
  memset(response_buffer, 0, 32);

  co2Serial.print(command);
  co2Serial.print("\r\n");
  
  num_bytes_received = co2Serial.readBytesUntil('\n', response_buffer, 32);
  if (num_bytes_received == 0)
  {
    debugSerial.println("ERROR - no response received from CO2 sensor.");
    return -1;
  }

  if (response_buffer[0] != command)
  {
    debugSerial.println("ERROR - invalid CO2 sensor data sent or received.");
    return -1; 
  }

  memcpy(response, response_buffer, strlen(response_buffer));
  return 0;
}

float get_co2_sensor_temperature_celsius(void)
{
  char response_buffer[32];
  memset(response_buffer, 0, 32);
  
  send_command_to_co2_sensor('t', response_buffer);

  int int_temperature = 0;
  float float_temperature = 0;
  sscanf(response_buffer, "t %d\r", &int_temperature);
  float_temperature = (int_temperature - 1000.0) / 10.0; 
  return float_temperature;
}

float get_co2_sensor_barometric_pressure_millibars(void)
{
  char response_buffer[32];
  memset(response_buffer, 0, 32);
  
  send_command_to_co2_sensor('B', response_buffer);

  int int_pressure = 0;
  float float_pressure = 0;
  sscanf(response_buffer, "B %d\r", &int_pressure);
  float_pressure = int_pressure;
  return float_pressure;
}

float get_co2_sensor_relative_humidity_percentage(void)
{
  char response_buffer[32];
  memset(response_buffer, 0, 32);
  
  send_command_to_co2_sensor('H', response_buffer);

  int int_humidity = 0;
  float float_humidity = 0;
  sscanf(response_buffer, "H %d\r", &int_humidity);
  float_humidity = int_humidity / 10.0;
  return float_humidity;
}

int get_co2_sensor_uncompensated_co2_ppm(void)
{
  char response_buffer[32];
  memset(response_buffer, 0, 32);
  
  send_command_to_co2_sensor('V', response_buffer);

  int int_co2 = 0;
  sscanf(response_buffer, "V %d\r", &int_co2);
  return int_co2 * 10;
}

int get_co2_sensor_compensated_co2_ppm(void)
{
  char response_buffer[32];
  memset(response_buffer, 0, 32);
  
  send_command_to_co2_sensor('Z', response_buffer);

  int int_co2 = 0;
  sscanf(response_buffer, "Z %d\r", &int_co2);
  return int_co2 * 10;
}

/*****************************************************************************
   Initialization
 ****************************************************************************/

void setup()
{
  /* Initialize serial ports */
  loraSerial.begin(LORA_SERIAL_BAUD);
  co2Serial.begin(CO2_SERIAL_BAUD);
  debugSerial.begin(DEBUG_SERIAL_BAUD);
  while (!debugSerial);

    /* Try to connect to TTN */
  debugSerial.println("--LORAWAN STATUS--");
  ttn.showStatus();
  debugSerial.println("--LORAWAN JOIN REQUEST--");
  ttn.join(appEui, appKey);
}

/*****************************************************************************
   Main Loop
 ****************************************************************************/

void loop()
{
  /* Update readings from the CO2 sensor */
  co2_sensor_state.temperature = get_co2_sensor_temperature_celsius();
  co2_sensor_state.humidity = get_co2_sensor_relative_humidity_percentage();
  co2_sensor_state.barometric_pressure = get_co2_sensor_barometric_pressure_millibars();
  co2_sensor_state.uncompensated_co2 = get_co2_sensor_uncompensated_co2_ppm();
  co2_sensor_state.compensated_co2 = get_co2_sensor_compensated_co2_ppm();

  /* Print out readings so we can see them */
  debugSerial.print("Temperature: ");
  debugSerial.print(co2_sensor_state.temperature);
  debugSerial.println("C");

  debugSerial.print("Relative Humidity: ");
  debugSerial.print(co2_sensor_state.humidity);
  debugSerial.println("%");

  debugSerial.print("Barometric Pressure: ");
  debugSerial.print(co2_sensor_state.barometric_pressure);
  debugSerial.println("mb");

  debugSerial.print("Uncompensated CO2: ");
  debugSerial.print(co2_sensor_state.uncompensated_co2);
  debugSerial.println("ppm");

  debugSerial.print("Compensated CO2: ");
  debugSerial.print(co2_sensor_state.compensated_co2);
  debugSerial.println("ppm");

  /* Send the CO2 sensor state to The Things Network */
  ttn.sendBytes((uint8_t*)&co2_sensor_state, sizeof(co2_sensor_state));
  debugSerial.println("");

  /* Wait 30 seconds in between readings */
  delay(30000);
}
