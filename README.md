Example code for talking to a MinIR-5% CO2 sensor MX board with an Arduino.

Sensor + MX Board:
https://www.co2meter.com/products/minir-co2-sensor?variant=51402030804

![enter image description here](https://cdn.shopify.com/s/files/1/0019/5952/products/CM-40330-MINIR5-pct-co2_354x.jpg?v=1508212941)